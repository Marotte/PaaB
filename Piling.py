import sqlite3
import sys
from hashlib import sha256

from BML import bmlify

class Piling:


    def __init__(self, dbfile = 'paab.sqlite', tsvfile = 'paab.tsv'):

        self.conn = sqlite3.connect(dbfile)
        self.cur  = self.conn.cursor()
        self.tsvfile = tsvfile

    def getPosts(self, last):
        
        self.cur.execute('SELECT * FROM last_post WHERE id > ?', (last,))
        posts = self.cur.fetchall()
            
        return posts

    def updateTSV(self):
        
        with open(self.tsvfile,'w') as f:
            
            for record in self.getPosts(0):

                f.write('\t'.join(list(map(str, record)))+'\n')

    def getUser(self, userid):
        
        lehash = sha256(userid.encode('utf-8')).hexdigest()
        username = self.cur.execute('SELECT username FROM mussel WHERE hash = ?', (lehash,)).fetchone()
        
        if username:
            
            return username[0]
            
        return lehash 

    def getUsername(self, userid):
        
        lehash = sha256(userid.encode('utf-8')).hexdigest()
        username = self.cur.execute('SELECT username FROM mussel WHERE hash = ?', (lehash,)).fetchone()
        
        if username:
            
            return username[0]
            
        return None 

    def addPost(self, post):
        
        post[1] = bmlify(post[1])
        post[2] = self.getUser(post[2])
        post[3] = bmlify(post[3])
        
        self.cur.execute('INSERT INTO post (time,info,login,message) VALUES (?,?,?,?)', post)
        self.conn.commit()
        self.updateTSV()

    def updateUser(self, userid, username):

        lehash = sha256(userid.encode('utf-8')).hexdigest()
        self.cur.execute('INSERT OR IGNORE INTO mussel (hash, username) VALUES (?,?)', (lehash, username[0:32]))
        self.cur.execute('UPDATE OR IGNORE mussel SET username = ? WHERE hash = ?', (username[0:32], lehash))
        self.conn.commit()
        return True
