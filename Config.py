

class Config(dict):

    def __init__(self):
    
        super().__init__()
        
        self['name']        = 'paab'
        self['dbfile']      = 'paab.sqlite'
        self['tsvfile']     = 'paab.tsv'
        self['msg_maxsize'] = 10000      # For the whole message, this includes at least 2052 bytes of overhead (empty UA, default login: '-', one letter post).
        self['pagetitle']   = 'PaaB'        
