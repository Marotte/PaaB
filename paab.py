from wsgiref.simple_server import make_server
from urllib.parse import unquote
from random import getrandbits
from sys import stderr

from PaaB import PaaB

board = PaaB(name=__name__)

def app(env, response):
    """
    WSGI callable
    """
    try:
        
        request_body_size = int(env.get('CONTENT_LENGTH', 0))
    
    except (ValueError):
        
        request_body_size = 0

    # Read and decode HTTP data into a dict
    data = {}
    request = env['wsgi.input'].read(request_body_size)
    fields = unquote(request.decode('utf8')).split('&')

    for d in fields:

        split = d.split('=')
        
        if len(split) == 2:
            
            k,v = split[0], split[1]
        
        else:
            
            k,v = split[0],''
        
        data[k] = v

    headers = []
    
    # Session cookie
    if 'HTTP_COOKIE' in env:
        
        cookies = env['HTTP_COOKIE'].split(';')
                
        try:
        
            for c in cookies:
                
                if c.split('=')[0].strip() == 'session_id':
        
                    session_id = c.split('=')[1].strip()
                    self.headers.append(('Set-Cookie','session_id='+session_id))

        except (IndexError, NameError) as e:
            
            print(str(e), file=stderr)
        
    else:    
    
        sid = '%032x' % getrandbits(128)
        headers.append(('Set-Cookie','session_id='+str(sid)))

    # ('', [('','')], '') = PaaB.process({}, {})
    (status, more_headers, content) = board.process(env, data)

    response(status, headers + more_headers)

    # Return response content as bytes.
    return content.encode('utf8')

if __name__ == "__main__":
    
    make_server('', 11000, app).serve_forever()

