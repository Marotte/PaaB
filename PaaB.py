import sqlite3
from pprint import pprint, pformat
from datetime import datetime
from time import time, ctime
from bs4 import BeautifulSoup
from random import getrandbits
from sys import path, stderr
from hashlib import sha256

from BML import bmlify
from Config import Config

config = Config()

class PaaB:

    default_name         = config['name']
    default_status       = '200 OK'

    def __init__(self, name = default_name, status = default_status):
    
        self.name         = name
        self.status       = status
        self.headers      = []
        self.content      = ''

    def process(self, env, data):

        #~ print(env, file=stderr)
        
        self.headers      = []
        
        skel      = """
                    <!DOCTYPE html>
                    <html>
                    <head>
                    <meta charset="UTF-8" />
                    <title></title>
                    <link rel="icon" href="favicon.ico" type="image/png" />
                    </head>
                    <body>
                    <noscript>Javascript requis.<br /></noscript>
                    </body>
                    </html>
                    """
        self.soup  = BeautifulSoup(skel,'html.parser')

        if env['REQUEST_URI'] == '/':
            
            self.headers.append(('Content-Type', 'text/html'))
            js = self.soup.new_tag('script')
            js.attrs['src'] = self.name+'/'+self.name+'.js'
            css = self.soup.new_tag('link')
            css.attrs = {'rel':'stylesheet', 'type':'text/css', 'href': self.name+'/'+self.name+'.css'}
            self.soup.head.contents += [js, css]
            self.soup.title.string = config['pagetitle']
            
            self.soup.body.attrs = {'onload':"""sendwait('I:0'); window.setInterval(send_last_id, 1000); set_listeners(); set_actions();
                                                window.setTimeout(getUsername,1000)"""}
            
            in1 = self.soup.new_tag('input')
            in1.attrs = {'type':'text' ,'id':'input' ,'name':'message' ,'value':''}
            in2 = self.soup.new_tag('input')
            in2.attrs = {'type':'button' ,'id':'post-submit' ,'name':'post-submit' ,'value':'Post' ,'size':'16',
                         'onclick':"""send('N:'+navigator.userAgent+'\t'+getVisitor()+'\t'+document.getElementById('input').value); clear_input()"""}
            br = self.soup.new_tag('br')             
            out1 = self.soup.new_tag('ol')
            out1.attrs = {'id':'chat'}
            
            profile_btn = self.soup.new_tag('input')
            profile_btn.attrs = {'id':'profile-btn', 'type':'button', 'value':'…'}
            
            profile = self.soup.new_tag('div')
            profile.attrs = {'id': 'profile-edit', 'class':'modal'}
            
            user_id = self.soup.new_tag('div')
            user_id.attrs = {'id':'user-id'}
            
            user_name_label = self.soup.new_tag('label')
            user_name_label.attrs = {'for':'user-name', 'id':'username-label'}
            user_name_label.string = 'Username: '
            
            user_name = self.soup.new_tag('input')
            user_name.attrs = {'id':'user-name', 'type':'text', 'value':''}
            
            close = self.soup.new_tag('input')
            close.attrs = {'id':'profile-close', 'type':'button', 'value':'✔'}
            
            tip = self.soup.new_tag('span')
            tip.string = 'Must be between 2 and 32 characters long. If the username you choose is already taken your current username won’t be modified.'
            
            content = self.soup.new_tag('div')
            content.attrs = {'class':'modal-content'}
            content.contents = [user_id, br, user_name_label, user_name, tip, br, close]

            profile.contents = [content]
            
            self.soup.body.contents = [in1,in2,profile_btn,br,out1,profile]
            
            # Visitor cookie
            if 'HTTP_COOKIE' in env:
                
                cookies = env['HTTP_COOKIE'].split(';')
                
                try:
                
                    for c in cookies:
                        
                        if c.split('=')[0].strip() == 'visitor_id':
                
                            visitor_id = c.split('=')[1].strip()
                            self.headers.append(('Set-Cookie','visitor_id='+visitor_id+';Expires='+ctime(time()+3214080000)))

                except IndexError as e:
                    
                    print(str(e), file=stderr)

        
            else:    
            
                sid = '%032x' % getrandbits(2048)
                self.headers.append(('Set-Cookie','visitor_id='+str(sid)+';Expires='+ctime(time()+3214080000)))

            self.content = self.soup.prettify()

        elif env['REQUEST_URI'] == '/tsv':
            
            self.headers.append(('Content-Type', 'text/tab-separated-values'))
            
            self.content = open(self.name+'.tsv','r').read()

        elif env['REQUEST_URI'] == '/post':
            
            self.content = ''
            visitor_id = None
            # Visitor cookie
            if 'HTTP_COOKIE' in env:
                
                cookies = env['HTTP_COOKIE'].split(';')
                
                try:
                
                    for c in cookies:
                        
                        if c.split('=')[0].strip() == 'visitor_id':
                
                            visitor_id = c.split('=')[1].strip()
                            self.headers.append(('Set-Cookie','visitor_id='+visitor_id+';Expires='+ctime(time()+3214080000)))

                except IndexError as e:
                    
                    print(str(e), file=stderr)
        
            if visitor_id and data['message']:
                
                post = []
                self.conn = sqlite3.connect(self.name+'.sqlite')
                self.cur  = self.conn.cursor()
                
                post.append(datetime.strftime(datetime.now(),'%Y%m%d%H%M%S'))
                post.append('Oxyure')
                post.append(sha256(visitor_id.encode('utf-8')).hexdigest())
                post.append(bmlify(data['message']))
                
                self.cur.execute('INSERT INTO post (time,info,login,message) VALUES (?,?,?,?)', post)
                self.conn.commit()
                self.cur.execute('SELECT * FROM last_post')
                posts = self.cur.fetchall()
                
                with open(self.name+'.tsv','w') as f:
            
                    for record in posts:

                        f.write('\t'.join(list(map(str, record)))+'\n')

        else:
            
            self.soup.body.attrs = {}
            self.soup.body.contents = []
            self.content = self.soup.prettify()

        return (self.status, self.headers, self.content)



        
