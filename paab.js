var ws = new WebSocket('wss://super.oxyure.com/paab-ws/');
var host = 'super.oxyure.com'
var lastid = 0;
var highlight_color = 'yellow';
var username = '';

function setView () {
    
    var chat = document.getElementById('chat');
    var h = parseInt(window.innerHeight * 92 / 100);
    chat.style.height = h+'px';
    
}

function getVisitor () {
    
    try {
        
        var ret = '';
        cookies = document.cookie.split(';');
        
        for (var i=0; i < cookies.length; i++) {
            
            c_name = cookies[i].split('=')[0].trim();
            c_val = cookies[i].split('=')[1].trim();

            if (c_name == 'visitor_id') {
                
                ret = c_val;
                
            }
            
        }
        
    } catch (e) {
        
        ret = '-';
        
    }
    
    return ret;
    
}

function send (data) {
    
    console.log('→ '+data);
    ws.send(data);
    
}

function getUsername () {
    
    send('W:'+getVisitor());
    
}

function waitForSocketConnection(socket, callback){
        setTimeout(
            function(){
                if (socket.readyState === 1) {
                    if(callback !== undefined){
                        callback();
                    }
                    return;
                } else {
                    waitForSocketConnection(socket,callback);
                }
            }, 5);
    };

function sendwait(msg) {
        waitForSocketConnection(ws, function() {
            ws.send(msg);
        });
    };

function date2norloge (date) {
    
    if (d >= new Date()) {
                
        d.setDate(d.getDate() - 1);
    }
        
    year  = String(d.getFullYear());
    month = String(d.getMonth() + 1).padStart(2,'0');
    day   = String(d.getDate()).padStart(2,'0');
    hour  = String(d.getHours()).padStart(2,'0');
    min   = String(d.getMinutes()).padStart(2,'0');
    sec   = String(d.getSeconds()).padStart(2,'0');
    
    return 'n'+year+month+day+hour+min+sec;
}

function render (shit) {
    
    l = shit.split(' ');
    var ret = null
    for (var i = 0; i < l.length; i++) {

        if (l[i].match('^[0-9]{2}:[0-9]{2}:[0-9]{2}$')) {
            
            l_l = l[i].split(':');
            
            d = new Date();
            d.setHours(l_l[0]);
            d.setMinutes(l_l[1]);
            d.setSeconds(l_l[2]);

            cls = date2norloge(d);
            
            l[i] = '<span class="'+cls+' noref" onmouseover="highlight(\''+cls+'\', true)" onmouseout="highlight(\''+cls+'\', false)">'+l[i]+'</span>';

        }
        
        else if (l[i].match('^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z?$')) {
            
            l_iso  = l[i].split('T');
            l_date = l_iso[0].split('-');
            l_time = l_iso[1].split(':');
            
            
            d = new Date();
            d.setFullYear(l_date[0]);
            d.setMonth(l_date[1] - 1);
            d.setDate(l_date[2]);
            d.setHours(l_time[0]);
            d.setMinutes(l_time[1]);
            d.setSeconds(l_time[2]);

            cls = date2norloge(d);
            
            l[i] = '<span class="'+cls+' noref" onmouseover="highlight(\''+cls+'\', true)" onmouseout="highlight(\''+cls+'\', false)">'+l[i]+'</span>';
            
        }
        
        else if (l[i].match('^#[0-9]{1,19}$')) {
            
            id = l[i].replace('#','id');
            l[i] = '<span class="'+id+' noref" onmouseover="highlight(\''+id+'\', true)" onmouseout="highlight(\''+id+'\', false)">'+l[i]+'</span>';
            
        }

    }

    return (l.join(' '))
    
}

function h(norloge, yesterday) {
    
    ss = norloge.charAt(norloge.length - 2)+norloge.charAt(norloge.length - 1)
    mm = norloge.charAt(norloge.length - 4)+norloge.charAt(norloge.length - 3)
    hh = norloge.charAt(norloge.length - 6)+norloge.charAt(norloge.length - 5)
    dd = norloge.charAt(norloge.length - 8)+norloge.charAt(norloge.length - 7)
    MM = norloge.charAt(norloge.length - 10)+norloge.charAt(norloge.length - 9)
    yy = norloge.charAt(norloge.length - 14)+norloge.charAt(norloge.length - 13)+norloge.charAt(norloge.length - 12)+norloge.charAt(norloge.length - 11)

    d = new Date();
    d.setHours(hh);
    d.setMinutes(mm);
    d.setSeconds(ss);
    d.setDate(dd);
    d.setMonth(MM - 1);
    d.setFullYear(yy);
    
    if (d < yesterday) {
                
        return (yy+'-'+MM+'-'+dd+'T'+hh+':'+mm+':'+ss);

    } else {

        return (hh+':'+mm+':'+ss);
        
    }
    
}


function highlight (cl, state, color) {
    
    if (color === undefined) {
        
        color = highlight_color;
        
    }
    
    elms = document.getElementsByClassName(cl);
    
    if (state) {
    
        for (var i = 0; i < elms.length; i++) {
            
            elms[i].style.backgroundColor = color;
            
        }
        
    } else {
        
        for (var i = 0; i < elms.length; i++) {
            
            elms[i].style.backgroundColor = 'transparent';
            
        }
        
    }
    
}

ws.onmessage = function (event) {

    console.log('← '+event.data);

    if (event.data.substr(0,2) == 'P:') {

        var newline = document.createElement('li');
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        
        s_data = event.data.substr(2);
        data = s_data.split('\t',5);
        newline.setAttribute('id','id'+data[0]);
        newline.setAttribute('class','id'+data[0]);
        cls = 'n'+data[1];
        human_norloge = h(data[1], yesterday);
        short_norloge = h(data[1])
        post_id       = 'id'+data[0]
        line = '<span class="n'+data[1]+' norloge" onmouseover="highlight(\''+cls+'\', true);highlight(\''+post_id+'\', true)"'
        line += ' onmouseout="highlight(\''+cls+'\', false);highlight(\''+post_id+'\', false)"'
        line += ' onclick="document.getElementById(\'input\').value += \'#'+data[0]+' \';document.getElementById(\'input\').focus()"';
        line += ' title="'+human_norloge+'">';
        line += short_norloge+'</span>';
        line += '<span class="login" title="'+data[2]+'">'+data[3]+'</span>';
        line += '<span class="message">'+render(data[4])+'</span>';
        newline.innerHTML = line;
        document.getElementById('chat').insertBefore(newline, document.getElementById('chat').firstChild);

    }
    
    else if (event.data.substr(0,2) == 'Y:') {

        username = event.data.substr(2);

    }
    
};

ws.onclose = function (event) {

    window.alert('Connection closed.');
    window.location.assign('https://'+host);

};

ws.onmerror = function (event) {

    window.alert(event.data);
    window.location.assign('https://'+host);
    

};


function last_id () {
    
    lastid = document.getElementById('chat').firstElementChild.id;
    
    return lastid;
    
}

function send_last_id () {

    ws.send('I:'+last_id().substr(2))
    
}

function clear_input () {
    
    document.getElementById('input').value = '';
    
} 

function set_listeners () {

document.getElementById('input')
    .addEventListener('keyup', function(event) {
    event.preventDefault();
    if (event.keyCode == 13 || event.which == 13) {
        document.getElementById('post-submit').click();
    }
});

}


function set_actions () {
    
var profile = document.getElementById('profile-edit');
var profile_btn = document.getElementById('profile-btn');
var user_id = document.getElementById('user-id');
var user_name = document.getElementById('user-name');
var close_profile = document.getElementById('profile-close');

close_profile.onclick = function() {
    
    if (user_name.value.trim() != '') {
    
        send('U:'+getVisitor()+'\t'+user_name.value.trim())
        
    }
    
    profile.style.display = 'none';
}

profile_btn.onclick = function() {
    
    user_name.value = username;
    user_id.innerHTML = '<b>Visitor ID:</b> '+getVisitor();
    profile.style.display = 'block';
}


// When the user clicks anywhere outside of the modal, close it without doing anything.
window.onclick = function(event) {
    if (event.target == profile) {
        profile.style.display = 'none';
    }
}

setView();

}

