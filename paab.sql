CREATE TABLE post (id INTEGER PRIMARY KEY, time INT, info TEXT, login TEXT, message TEXT);
CREATE VIEW last_post AS SELECT id,time,info,login,message FROM post ORDER BY time DESC, id DESC LIMIT 200;
CREATE TABLE mussel (hash TEXT UNIQUE, username TEXT UNIQUE);
CREATE TRIGGER trimer AFTER INSERT ON post
BEGIN
     DELETE FROM post WHERE id < ((SELECT max(id) FROM post) - 65535);
END;



