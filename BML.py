import re
from html import escape, unescape
from sys import stderr

def bmlify(wild_shit):
    
    r = escape(re.sub('\s+',' ',wild_shit.replace('\u00A0',' ').replace('\t',' ')))

    ok_tags = ['b','i','s','code','tt']
    url = re.compile('(http|ftp|https)://.*')
    tokens = []
    
    for t in ok_tags:
        
        r = r.replace('&lt;'+t+'&gt;','<'+t+'>')
        r = r.replace('&lt;/'+t+'&gt;','</'+t+'>')
    
    for token in r.split():

        nt = token

        if url.match(token):
                
                nt = '<a href="'+token.replace('&amp;','&').replace('[','%5B').replace(']','%5D')+'">&lsqb;url&rsqb;</a>'
                tokens.append(nt)
                continue

        else:
            
            tokens.append(nt)

    
    return(' '.join(tokens))


