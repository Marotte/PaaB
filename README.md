# Python as a Board

This board is composed of two separated [uWSGI](https://uwsgi-docs.readthedocs.io/en/latest/) applications.

You must manually create the database, alongside the script by default, with the provided SQL script:

    $ sqlite3 paab.sqlite < paab.sql
    
The file must be readable and writable by the uWSGI user (ex: _www-data_).