import uwsgi
from sys import stderr
from datetime import datetime

from Piling import Piling
from Config import Config

config = Config()

def app(env, start_response):
    
    last_id     = 0
    dbfile      = config['dbfile']
    tsvfile     = config['tsvfile']
    msg_maxsize = config['msg_maxsize']
    
    piling      = Piling(dbfile, tsvfile)

    try:
    
        # complete the handshake
        uwsgi.websocket_handshake(env['HTTP_SEC_WEBSOCKET_KEY'], env.get('HTTP_ORIGIN', ''))
        while True:

            enc_msg = uwsgi.websocket_recv()
            
            if len(enc_msg) > msg_maxsize:
                
                continue
                
            msg = enc_msg.decode('utf-8')
            #~ print(msg, file=stderr)

            if msg[1] is not ':':

                continue

            if msg[0] is 'I':
                
                try:
                
                    last_id = int(msg[2:])
                    # if any newer posts than last_id, send them
                    for p in reversed(piling.getPosts(last_id)):
            
                        #~ print(type(p), file=stderr)
                        s_p = '\t'.join(map(str,p))
                        uwsgi.websocket_send('P:'+s_p) 
                    
                except Exception as e:
                    
                    #~ print(str(e), file=stderr)
                    continue

            elif msg[0] is 'N':
            
                post = msg[2:].split('\t',2)
                
                if not post[2]:
                    
                    continue
                    
                nor  = datetime.strftime(datetime.now(),'%Y%m%d%H%M%S')
                post.insert(0, nor)
                piling.addPost(post)

            elif msg[0] is 'U':
            
                userid = msg[2:].split('\t',1)[0]
                username = msg[2:].split('\t',1)[1]
                
                if not username or len(username) < 2:
                    
                    continue

                piling.updateUser(userid, username)

            elif msg[0] is 'W':
            
                username = piling.getUsername(msg[2:])
                
                if not username or username == '-':
                    
                    continue

                uwsgi.websocket_send('Y:'+username)

    except OSError as e:
            
        print(str(e), file=stderr)

